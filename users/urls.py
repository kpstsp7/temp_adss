# users/urls.py
from django.urls import path
from . import views

urlpatterns = [
    path('', views.SignUp.as_view(), name='signup'),
    path('success', views.SuccessPage.as_view(), name='success'),
    path('singupadw', views.SignUpAdwAccount.as_view(), name='signup_adw'),
]