from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.core.exceptions import ValidationError
from .models import CustomUser, AdwUser
import re


class CustomUserCreationForm(UserCreationForm):
    """docstring for UserCreationForm"""
    class Meta(UserCreationForm):
        model = CustomUser
        fields = ('username', 'email')


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = UserChangeForm.Meta.fields


class CustomSignUpForm(UserChangeForm):
    class Meta:
        model = AdwUser
        fields = ('email',)

class SuccessForm(UserChangeForm):
    class Meta:
        model = AdwUser
        fields = ('email',)
        

class CustomSignUpFormAdw(UserChangeForm):
    class Meta:
        model = AdwUser
        fields = ('account',)

    def clean(self):
    	cleaned_account = re.sub(r"-", "", self.cleaned_data['account'])
    	if not cleaned_account.isdigit() or len(cleaned_account)<6:
    		raise ValidationError(('Please enter your adword account correctly'))
        

        