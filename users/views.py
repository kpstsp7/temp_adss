from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.views import generic
from .forms import CustomSignUpForm, CustomSignUpFormAdw, SuccessForm
from .models import AdwUser
import jsonify
from googleads import ad_manager, adwords
from googleads import oauth2
import google.oauth2.credentials
import google_auth_oauthlib.flow
import google.ads.google_ads.client
from django.http import HttpResponseRedirect, HttpResponse
from django.template.response import TemplateResponse
from .googlesetting import KEY_FILE, APPLICATION_NAME, GOOGLEADSCONF
import datetime
import re
import yaml


PAGE_SIZE = 500

def read_conf(cpath):

    try:
        with open(cpath, 'r') as stream:
            data_loaded = yaml.load(stream)
    except OSError as e:
        print("\n FATAL Error: Configuration file {} not found.".format(cpath))
        sys.exit()


    return data_loaded

class SuccessPage(generic.CreateView):
	form_class = CustomSignUpFormAdw
	template_name = 'success.html'
	queryset = AdwUser.objects.all()

	def get(self, request, template_name="success.html"):
		args = {}
		link_result =  self.request.session['res']
		args['link_result'] = link_result
		return TemplateResponse(request, template_name, args)




class SignUpAdwAccount(generic.CreateView):
	form_class = CustomSignUpFormAdw
	template_name = 'signup_adw.html'
	success_url = reverse_lazy('success')


	def form_valid(self, form):

		res = "SUCCESS: Account invite sent, please check your email."
		
		user_account = re.sub(r"-", "", form.cleaned_data['account'])

		print(user_account)

		mng_client = adwords.AdWordsClient.LoadFromStorage(GOOGLEADSCONF)
		managed_customer_service = mng_client.GetService('ManagedCustomerService', version='v201809')
		today = datetime.datetime.today().strftime('%Y%m%d %H:%M:%S')

		googleadsconf = read_conf(GOOGLEADSCONF)

		manager_id = re.sub(r"-", "", googleadsconf['adwords']['client_customer_id'])

		operation = [{
		    		'operator': 'ADD',
		    		'operand': {
		        	# 'managerCustomerId': str(self.request.session['root_adw_id']), #242-538-1549
		        	'managerCustomerId': manager_id, # '2395865684' ,#'3893187601', #str(self.request.session['root_adw_id']),  #'2425381549', 	# 
		        	'clientCustomerId':  user_account,# '2425381549' ,#'2430889684' ,#'2425381549',  #'5009410200', #'2425381549', #str(credentials.client_id),
		        	'linkStatus': 'PENDING'
		        	# 'pendingDescriptiveName': 'Some text that helps identifying the invitation',
		        	# 'isHidden': False  # This can be used to hide the account in your MCC to decrease clutter
		    		}
				}]

		# Most errors covered
		try:
			response = managed_customer_service.mutateLink([operation])
			print(response.links[0])
		except Exception as e:
			if "ALREADY_INVITED_BY_THIS_MANAGER" in str(e):
				res = "ERROR : Account number ALREADY_INVITED_BY_THIS_MANAGER"
				print(e)
			elif "TOO_HIGH" in str(e): 	#"TOO_HIGH" in str(e):
				res = "ERROR : Wrong account number (Enum: TOO_HIGH)"
				print(e)
			elif "TOO_LOW" in str(e):
			 	res = "ERROR : Wrong account number (Enum: TOO_LOW)"
			 	print(e)
			elif "TEST_ACCOUNT_LINK_ERROR" in str(e):
			 	res = "ERROR : You cannot link this account to current MCC (cause: trying link MIXED TEST and PRODUCT ACCOUNT)"
			 	print(e)
			elif "ALREADY_MANAGED_BY_THIS_MANAGER" in str(e):
			 	res = "ERROR : This account already managed by this manager"
			 	print(e)	
			elif "BAD_ID" in str(e):
				res = "ERROR: Incorrect account number"
			else:
				res = "ERROR: Unknown Error"


		self.request.session['res'] = res


		self.object = None

		form.save()

		return HttpResponseRedirect(reverse('success'))



class SignUp(generic.CreateView):
    form_class = CustomSignUpForm
    template_name = 'signup.html'


    def get(self, request, *args, **kwargs):
    	# put your get code herev201809
    	return super().get(request, *args, **kwargs)



    def DisplayAccountTree(self, account, accounts, links, depth=0):
	    """Displays an account tree.
	  Args:
	    account: dict The account to display.
	    accounts: dict Map from customerId to account.
	    links: dict Map from customerId to child links.
	    depth: int Depth of the current account in the tree.
	  """
	    prefix = '-' * depth * 2
	    print('%s%s, %s' % (prefix, account['customerId'], account['name']))
	    if account['customerId'] in links:
	        for child_link in links[account['customerId']]:
	            child_account = accounts[child_link['clientCustomerId']]
	            self.DisplayAccountTree(child_account, accounts, links, depth + 1)

	


    def form_valid(self, form):
        
        print(form.cleaned_data['email'])
        user_mail = form.cleaned_data['email']

        # oauth2_client = oauth2.GoogleServiceAccountClient( \
			# KEY_FILE, oauth2.GetAPIScope('ad_manager'))

        # ad_manager_client = ad_manager.AdManagerClient(oauth2_client, APPLICATION_NAME)
        client = adwords.AdWordsClient.LoadFromStorage(GOOGLEADSCONF)
        managed_customer_service = client.GetService('ManagedCustomerService', version='v201809')
        today = datetime.datetime.today().strftime('%Y%m%d %H:%M:%S')

# Get root account id

        # Construct selector to get all accounts.
        offset = 0
        selector = {
            'fields': ['CustomerId', 'Name'],
            'paging': {
                'startIndex': str(offset),
                'numberResults': str(PAGE_SIZE)
            }
         }
        more_pages = True
        accounts = {}
        child_links = {}
        parent_links = {}
        root_account = None

        while more_pages:
        	# Get serviced account graph.
        	page = managed_customer_service.get(selector)
        	if 'entries' in page and page['entries']:
        		if 'links' in page:
        			for link in page['links']:
        				if link['managerCustomerId'] not in child_links:
        					child_links[link['managerCustomerId']] = []
        				child_links[link['managerCustomerId']].append(link)
        				if link['clientCustomerId'] not in parent_links:
        					parent_links[link['clientCustomerId']] = []
        				parent_links[link['clientCustomerId']].append(link)
        		for account in page['entries']:
        			accounts[account['customerId']] = account
        	offset += PAGE_SIZE
        	selector['paging']['startIndex'] = str(offset)
        	more_pages = offset < int(page['totalNumEntries'])

        # Find the root account.
        for customer_id in accounts:
        	if customer_id not in parent_links:
        		root_account = accounts[customer_id]

        # Display account tree.
        if root_account:
        	print('CustomerId, Name')
        	self.DisplayAccountTree(root_account, accounts, child_links, 0)
        else:
        	print('Unable to determine a root account')

        # 
        # jsonify(root_account)
        self.request.session['root_adw_id'] = root_account.__values__['customerId']



        # print(self.request.session.get('root_adw_id'))
        print(self.request.session['root_adw_id'])


        form.save()
        return HttpResponseRedirect(reverse_lazy('signup_adw'))
        



