from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models
import re


class CustomUserManager(UserManager):
    pass


class CustomUser(AbstractUser):
    objects = CustomUserManager()


class AdwUser(models.Model):

    # email = models.CharField(max_length=255, blank=True)
    email = models.EmailField(max_length=70, null=True, blank=True, unique=True)
    signup_date = models.DateField(auto_now_add=True)
    status = models.BooleanField(default=False)
    account = models.CharField(max_length=255)
    adw_mcc_status = models.CharField(max_length=255, default="NOT_ATTACHED") # PENDING, LINKED, NOT_ATTACHED 



