##SignUp Form

###Main requirements:

	Django==2.1.4
	django-allauth==0.38.0
	google-ads==0.5.0
	google-api-core==1.7.0
	google-auth==1.6.1
	google-auth-oauthlib==0.2.0
	googleads==15.0.1
	googleapis-common-protos==1.5.5
	...

install requirements

	pip install -r requirements.txt


###Configuration:

#####DB Connection in Django settings

Should be configure for Postgres, using standard django settings section.

	DATABASES = {
	    'default': {
		'NAME': 'adsvsup',
		'USER': 'adsvsup',
		'PASSWORD': '123123',
		'HOST': 'localhost',
		'PORT': '',
		    }
	}
	
#####Django allauth settings

Don't important, will be removed in final version


#####googleads.yaml

Need fill only 'adwords' section
	Important: *client_customer_id* should contain your MCC root account to wich you plan link invited accounts
	
#####googlesettings.py

	Deprecated/will be removed : KEY_FILE 
	Deprecated/will be removed : APPLICATION_NAME
	GOOGLEADSCONF = "./googleads.yaml" #Placement of main credentials file for googleads


###Usage

First run DB migrations in project directory :

	python manage.py makemigration
	python manage.py migrate
	
Run dev server:

	python manage.py runserver
	
	
Start url : *http://127.0.0.1:8000*
	
First stage need input email. Second is need enter Adwords account number, if dashes been used - they will be removed. On next page will be shown results sending invite to user. If account already invited, will be shown related error.




######TODO: add all response ERRORS for wrong customer_Ids : ALREADY MANAGED, BAD_ID, AUTHORISATION DENIED, etc 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	